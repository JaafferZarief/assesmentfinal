import React from 'react';
import { Link } from 'react-router-dom';
import logo from "../images/logo.png"
import './Home.css';

const mystyle={
  background: 'linear-gradient(to right, #c2fcce 60%,white 40%)',
  height: '100vh',
  color: '#09426e'
}


function Home() {
  return (
    <div style={mystyle}>
      <img src={logo} style={{width:'70px',height:'70px',margin:'30px'}}/>
      <div className='style'>
        <h1>
          Welcome to<br></br>
          <span style={{color:'#ff6912'}}>Crystal Delta</span><br></br>
          e-learinig
        </h1>
      </div>
      <div className='login'>
      <form style={{padding:'20px'}}>
    <div class="form-group">
      <h5>Login to your account</h5>
      <br></br>
      <label for="exampleInputEmail1">Customer ID</label><br></br>
      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"></input>
    </div><br></br>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"></input>
    </div>
    <br></br>
    <button type="submit" class="btn btn-info"><Link style={{textDecoration:'none'}} to='/course'>Submit</Link></button>
    </form>
        </div>
    </div>
  );
}

export default Home;
