import React from "react";
import "./Courses.css"
import img1 from "../images/coding.jpg"
import img2 from "../images/reactjs.jpg"
import logo from "../images/logo.png"
import { Link } from "react-router-dom"

function Courses() {
    return (
        <div style={{backgroundColor:'#e6f6f7',color:'#09426e'}}>
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
                <a class="navbar-brand" href="#"><img src={logo} style={{width:'50px',height:'50px'}}/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#"  ><Link style={{color:'orange'}} to='/course'>All Courses</Link></a>
                        <a class="nav-item nav-link active" href="#" ><Link style={{color:'black',textDecoration:'none'}} to='/mydashboard'>My Dashboard</Link></a>
                    </div>
                </div>
                <button className="btn btn-outline-primary"><Link style={{textDecoration:'none'}} to='/'>Logout</Link></button>
            </nav>
            <br></br>
        <div className="container">
            <h2>Greetings Shyam,</h2><br></br>
            <h5>Take a look into the list of all courses</h5><br></br>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img2} alt="" />
                            <div className="card-body">
                                <h5 className="card-title"><Link style={{textDecoration:'none',color:'#09426e'}} to='/react'>React js Basic</Link></h5>
                                <p className="card-text">Course ID:CDL001</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img1} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Node js Basic</h5>
                                <p className="card-text">Course ID:CDL002</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img2} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Python Tutorial</h5>
                                <p className="card-text">Course ID:CDL003</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img1} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Automation Testing</h5>
                                <p className="card-text">Course ID:CDL004</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img2} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Agile Guide</h5>
                                <p className="card-text">Course ID:CDL005</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img1} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Devops Manual</h5>
                                <p className="card-text">Course ID:CDL006</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img2} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Java & Spring Boot</h5>
                                <p className="card-text">Course ID:CDL007</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                    <div className="card" style={{ width: '18rem' }}>
                            <img className="card-img-top" src={img1} alt="" />
                            <div className="card-body">
                                <h5 className="card-title">Angular Tutorial</h5>
                                <p className="card-text">Course ID:CDL008</p>
                                <div class="d-flex">
                                    <p>Start Date <br></br> 19-12-2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p>End Date <br></br> 15-06-2021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}
export default Courses