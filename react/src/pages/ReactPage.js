import React from "react";
import img from "../images/reactjs.jpg"
import logo from "../images/logo.png"
import { Link } from "react-router-dom"
const mystyle ={
    border:'0px solid #e6f6f7',
    width:'660px',
    backgroundColor:'#e6f6f7',
    height:'100%',
    color:'#09426e'

}



class ReactPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {isenroll:true};
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        this.setState(prevstate => ({
            isenroll : !prevstate.isenroll
        }));
    }
    render(){

    return(
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
                <a class="navbar-brand" href="#"><img src={logo} style={{width:'50px',height:'50px'}}/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#" ><Link style={{color:'orange'}} to='/course'>All Courses</Link></a>
                        <a class="nav-item nav-link active" href="#" ><Link style={{color:'black',textDecoration:'none'}} to='/mydashboard'>My Dashboard</Link></a>
                    </div>
                </div>
                <button className="btn btn-outline-primary"><Link style={{textDecoration:'none'}} to='/'>Logout</Link></button>
            </nav>
        <div style={{ backgroundImage: `url('${img}')`, backgroundRepeat: 'no-repeat',backgroundSize:'100% 100%',height:'100vh' }}>
                <div style={mystyle}>
                    <div className="container">
                    <br></br>
                    <br></br>
                    <h2>React js Basic</h2>
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <p>Instructors : John Doe,Steve Rogers</p>
                                <p>Duration : 19 hours</p>
                            </div>
                            <div className="col">
                                <p>Course Starts : 10-12-2020</p>
                                <p>Course Ends : 15-06-2021</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <br></br>
                        <p>React Makes it paintless to create interactive UIs.Design simple views for each state in your application, and React will efficientlyupdate and render just the right components when your data chnages.</p>
                        <p>Declarative views make your code more predictable and easier to debug.Build encapsulated components that manage their own state,then compose them to make complex UIs.</p>
                        <p>Since component logic is wriiten in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.</p>
                    </div>
                    <br></br>
                    <br></br>
                    <p style={{textAlign:'center'}}><button className="btn btn-info" onClick={this.handleClick}>{this.state.isenroll ? 'Enroll Now':'Unenroll Now'}</button></p>
                </div>
                </div>
        </div>
        </div>
    );
    }
}

export default ReactPage;